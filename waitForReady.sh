#!/bin/sh

: ${SLEEP_TIME:=1}     # Default SLEEP: 1 seconds
: ${MAX_WAIT_TIME:=30} # Default MAX TOTAL WAIT: 30 seconds

wait_for_ready() {
  echo "Waiting for $1 to be ready on port $2 (max ${MAX_WAIT_TIME} seconds)..."

  waitTime=0
  while ! curl -s ${1}:${2} > /dev/null 2>&1 ; do
      # Check for timeout
      waitTime=$((waitTime + SLEEP_TIME))
      if [ ${waitTime} -gt ${MAX_WAIT_TIME} ]; then
          echo "Max wait time (${MAX_WAIT_TIME}s) exceeded"
          return 1
      fi
    echo "Not ready - sleeping ${SLEEP_TIME}s"; sleep $SLEEP_TIME
  done

  echo 'Service is ready!'
}

# Check for each service existing with error in case of timeout
set -e
for var in "$@"
do
  host=${var%:*}
  port=${var#*:}
  wait_for_ready $host $port
done
