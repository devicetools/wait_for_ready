# README #

### Scope ###

This repository contains a script that makes an HTTP request to a given host:port and exits after it gets a response - irrespective from what that response is.
The intention for this is to be used in our CircleCI builds within the docker-compose.yml and Makefile files, as described below.

### Usage ###

Steps:

- Build a docker image from it named: devicetools/wait_for_ready
 
- Add the following code to docker-compose, where api is the name of the docker service that we will be waiting for to be ready:
```
    start_dependencies:
        image: devicetools/wait_for_ready
        depends_on:
          - api
        command: api:80
```
- in the Makefile make sure you have these lines in the "test" task:

```
	docker-compose ${FILE_OPTION} up -d api  # Fire up the api
	docker-compose ${FILE_OPTION} run start_dependencies # Wait for the api to be up
	docker-compose ${FILE_OPTION} run behavior_tests # Run the behavior tests
```