FROM alpine:3.6

COPY waitForReady.sh /

RUN apk add --update ca-certificates && update-ca-certificates
RUN apk update && apk upgrade && apk add bash
RUN apk add curl

ENTRYPOINT ["/waitForReady.sh"]
